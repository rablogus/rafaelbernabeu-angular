import { Component, OnInit } from '@angular/core';
import { User } from '../entity/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users:Array<User> = [
    {
    "id":1,
    "name":"Rafael"
    },
    {
    "id":2,
    "name":"Bernabeu"
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
