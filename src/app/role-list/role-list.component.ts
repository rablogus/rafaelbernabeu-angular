import { Component, OnInit } from '@angular/core';
import { Role } from '../entity/role';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {

  roles:Array<Role> = [
    {
    "id":1,
    "name":"Admin"
    },
    {
    "id":2,
    "name":"User"
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}