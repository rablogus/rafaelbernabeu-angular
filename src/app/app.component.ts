import { Component, NgModule } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rafaelbernabeu-angular';
  user = 'Meu NgModule';
  
  value:number = 2;
  value2:number = 5;
  total:number = this.value + this.value2;

  public somar() {
    this.total = this.value + this.value2;
  }
}
